#!/bin/bash

#lcov --directory . --capture --output-file heat.lcov
#lcov_cobertura.py heat.lcov --output heat-coverage.xml
gcovr -r . -x -o heat-coverage.xml

export DEFINITIONS=""
export CPPCHECK_INCLUDES="-I. -Iinclude"
export SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
export SOURCES_TO_ANALYZE="."

cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml

rats -w 3 --xml ${SOURCES_TO_ANALYZE} > heat-rats.xml

bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.c\|.*\.h" | vera++ - -showrules -nodup |& vera++Report2checkstyleReport.perl > heat-vera.xml'

mpirun "-np" "4" valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes --suppressions=/usr/share/openmpi/openmpi-valgrind.supp --suppressions=tools/heat-valgrind.supp "./build/heat_par" "10" "10" "200" "2" "2" "0"
