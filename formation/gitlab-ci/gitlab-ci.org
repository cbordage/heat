#+TITLE: GitLab CI
#+AUTHOR: Inria BSO-SED
#+LANGUAGE:  en
#+OPTIONS: H:3 num:t \n:nil @:t ::t |:t _:nil ^:nil -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil pri:nil tags:not-in-toc html-style:nil
#+BEAMER_THEME: Rochester
#+HTML_HEAD:   <link rel="stylesheet" title="Standard" href="css/worg.css" type="text/css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="css/VisuGen.css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="css/VisuRubriqueEncadre.css" />

GitLab is a Git-based fully featured platform for software development
and Inria hosts its own [[https://gitlab.inria.fr/][server]]. It offers a [[https://about.gitlab.com/features/gitlab-ci-cd/][continuous integration]]
service, simply called "gitlab-ci", which makes our software testing
convenient because it is very well integrated into GitLab.

In this presentation we will guide users in the first steps to setup
gitlab-ci pipelines in order to automatize tests when git branches are
pushed to the server. We will explain how to configure "runners",
machine that will actually execute builds and tests, then how to write
the .gitlab-ci.yml file which contains the jobs definitions.  More
specifically we will discuss how to:
- execute scripts in parallel or as a sequence (pipeline),
- save some of the data generated (artifacts/cache),
- use tags to link jobs to machines,
- use docker image,
- define when jobs are created (branches filter, in case of success or
  failure),
- keep your online documentation up-to-date with pages.

This document is hosted on the *heat* gitlab project,
cf. https://gitlab.inria.fr/sed-bso/heat, see the file
~formation/gitlab-ci/gitlab-ci.org~ (you should be logged in
https://gitlab.inria.fr/).

* Introduction
  GitLab CI is directly integrated in GitLab, contrary to Jenkins for
  which you need to play with plugins.

  Userdoc:
    - GitLab Inria: https://gitlab.inria.fr/help/ci/README.md
    - GitLab Community: https://docs.gitlab.com/ce/ci/quick_start/

  What for?
    - merge request or push triggers your *CI pipeline*
    - makes it easy to see whether a merge request/push caused any of
      the tests to fail before you even look to the code.
    - continuous delivery and continuous deployment allow to
      automatically deploy tested code to staging and production
      environments
* Getting started
  The steps needed to have a working CI can be summed up to:
  1. Add ~.gitlab-ci.yml~, where you define your tests jobs, to the root
     directory of your repository
  2. Enable *Pipelines* in your project's settings
  3. Configure one or several runners which will actually run the
     tests
** First config file .gitlab-ci.yml
   You need to create a YAML file named .gitlab-ci.yml in the root
   directory of your repository.

   The YAML file defines a set of jobs with constraints stating when
   they should be run. The jobs are defined as top-level elements with
   a name and always have to contain at least the script clause:
   #+begin_example
   job1:
     script: "execute-script-for-job1" # ex: ./configure;make;make install

   job2:
     script: "execute-script-for-job2" # ex: test.sh
   #+end_example
   Jobs are picked up by Runners and executed within the environment
   of the Runner. What is important, is that *each job is run
   independently from each other* (different build directory for
   example). To create dependent jobs, see [[https://gitlab.inria.fr/help/ci/pipelines.md][pipelines]].

   Create the file, for example here we define a job "test" that
   execute the commands ~hostname~ and ~whoami~ in a shell on the runner
   #+begin_example
   test:
     script:
       - hostname
       - whoami
   #+end_example
   Then push it to your repository
   #+begin_example
   git add .gitlab-ci.yml
   git commit -m "Add .gitlab-ci.yml"
   git push origin master
   #+end_example
   Recall that we don't have defined any runners yet such that the job
   is not executed anywhere for now.

** Enable Pipelines
   To be able to configure runners, you have to enable *Pipeline* first in
   the configuration of your projet.
   - go to the *General* *Settings* of your project
   - go to the *Permissions* section
   - enable *Pipelines* (/Build, test, and deploy your changes/)
   - clic on "Save changes"

** Runner installation
   See https://docs.gitlab.com/runner/ and
   https://gitlab.inria.fr/siteadmin/doc/wikis/faq#installing-runners.

   In GitLab, Runners run the jobs that you define in
   .gitlab-ci.yml. A Runner can be a virtual machine, a VPS, a
   bare-metal machine, a docker container or even a cluster of
   containers. GitLab and the Runners communicate through an API, so
   the only requirement is that the Runner's machine has Internet
   access.

   Now, you should be able to register a runner. Please look at the
   instructions about the runners configuration in your project
   settings
    - go to the Settings of your project then, CI/CD, then Runners
    - read the instructions at the left side *Specific Runners*, note
      1. the URL that you will use during the runner registration
         https://gitlab.inria.fr/
      2. and the project registration token

   We will
   1. connect to a machine which will execute the tests or
      consider the current machine you are logged in
   2. install the necessary "gitlab-runner" program
   3. register a runner /i.e./ make the machine available as a test
      executor for a given gitlab project

   Let's give an example with a Linux Debian OS. Connect to such a
   machine then follow the instructions here
   http://docs.gitlab.com/runner/install/ to install the
   "gitlab-runner" program
   #+begin_src
   sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
   sudo chmod +x /usr/local/bin/gitlab-runner
   sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
   sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
   sudo gitlab-runner start
   #+end_src

   Read the instruction to register the runner (make the link between
   the runner and the project)
   http://docs.gitlab.com/runner/register/index.html.

   Register the runner. An example
   #+begin_src
   sudo gitlab-runner register
   Running in system-mode.

   Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
   https://gitlab.inria.fr/
   Please enter the gitlab-ci token for this runner:
   # copy/paste the project's secret token here
   Please enter the gitlab-ci description for this runner:
   [ubuntu1604]: testgitlab-ubuntu1604
   Please enter the gitlab-ci tags for this runner (comma separated):
   ci.inria.fr, linux, ubuntu 18.04 amd64, heat
   Registering runner... succeeded                     runner=Ae3jUcaw
   Please enter the executor: virtualbox, docker-ssh+machine, docker, parallels, ssh, docker+machine, kubernetes, docker-ssh, shell:
   shell
   Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
   #+end_src

   If you go back in the Runners settings of your project, you should
   see a new runner available in the *Runners activated for this
   project* part with a green circle.

   Now you can test a CI job by commit+push something or directly by
   hand in the gitlab interface, see CI/CD -> Pipelines -> Run
   Pipeline green button.

*** gitlab-runner + Docker
    Install docker on your machine, e.g.
    #+begin_src
    sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker
    #+end_src
    When [[http://docs.gitlab.com/runner/register/index.html][registering]], select docker as executor.

    Then, in your ~.gitlab-ci.yml~ specify either globally the image to
    use for every jobs, e.g.
    #+begin_src
    image: ubuntu

    job1:
      script: ls -alrt

    job2:
      script: whoami
    #+end_src
    or for some specific jobs only
    #+begin_src
    job1:
      script: ls -alrt

    job2:
      image: ubuntu
      script: whoami
    #+end_src

    Read more about [[https://gitlab.inria.fr/help/ci/docker/README.md][gitlab-ci]] with Docker.

*** Additional information about runners
**** Shared vs. Specific runners
     A Runner that is specific only runs for the specified project. A
     shared Runner can run jobs for every project that has enabled the
     option Allow shared Runners. You can only register a Shared Runner
     if you have admin access to the GitLab instance. We are not admin
     so that we do not consider shared Runners.

**** Specific runners can be shared between projects
     You can set up a specific Runner to be used by multiple
     projects. The difference with a shared Runner is that you have to
     enable each project explicitly for the Runner to be able to run
     its jobs.

     To share the runner, be sure it is not "Protected", see the Edit
     button of the runner. Then in any of your project your are
     "Maintainer" of you should be able to enable the runner in
     Settings -> CI/CD -> Runners.

**** Lock a specific Runner from being enabled for other projects
     You can configure a Runner to assign it exclusively to a
     project. When a Runner is locked this way, it can no longer be
     enabled for other projects. This setting is available on each
     Runner in Project Settings > CI/CD > Runners settings.

**** Are the runners shared when project is forked?
     Specific Runners do not get shared with forked projects
     automatically. A fork does copy the CI settings (jobs, allow
     shared, etc) of the cloned repository.

**** gitlab-runner configuration file
     Some advanced parameters can be configured by editing the
     config.toml file see [[https://docs.gitlab.com/runner/configuration/advanced-configuration.html][advanced configuration]].
     Example of parameters:
     - concurent: limits how many jobs globally can be run concurrently
     - environment: append or overwrite environment variables
     - shell: the name of shell to generate the script (default value is platform dependent)
     - builds_dir: directory where builds will be stored
     - cache_dir: directory where build caches will be stored
     - output_limit: set maximum build log size in kilobytes, by default set to 4096 (4MB)

**** Limit CI jobs to be run concurently on a machine hosting the gitlab-runner service
     See variables "concurrent" and "limit"
     https://docs.gitlab.com/runner/configuration/advanced-configuration.html

**** Note about the gitlab build permission model
     https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html

*** Unregister runners
    Connect to the machine where registering occurs
    #+begin_src
    # list runners registered
    sudo gitlab-runner list
    # check runners registered
    sudo gitlab-runner verify
    # unregister one specific runner
    sudo gitlab-runner unregister -t ...
    # unregister all runners
    sudo gitlab-runner unregister --all-runners
    #+end_src
    Or directly edit the ~/etc/gitlab-runner/config.toml~ file.

* Configuration of the jobs with the file .gitlab-ci.yml
  https://gitlab.inria.fr/help/ci/yaml/README.md

  We will illustrate jobs configuration using the existing gitlab
  project, [[https://gitlab.inria.fr/sed-bso/heat][heat]].

  You can fork the project if you want to test the jobs from your
  side. For that, go to the [[https://gitlab.inria.fr/sed-bso/heat][heat project]] page and clic on *Fork*. You
  can also directly try on your own project.

** define independent jobs

   Let's consider we have a project with a very basic .gitlab-ci.yml
   file such as the previous and first example
   #+begin_example
   test:
     script:
       - hostname
       - whoami
   #+end_example

   We propose now to define build jobs for the heat project. We want
   for example to test that the project builds and runs with the
   default configuration. Of course the prerequisites of the heat
   project should be available in the environment of the runner
   (e.g. your own machine). The minimal configuration requires a C
   compiler such as GCC and CMake. Optionally you will need an MPI
   implementation, e.g. OpenMPI.

   We suppose that we have one runner enabled for this project
   installed on a Linux system and that this runner *can run untagged
   jobs* (see the /edit/ button of the runner). One can define the file
   .gitlab-ci.yml as follows
   #+begin_example
   linux:
     script:
       - cmake .
       - make
       - ctest
   #+end_example
   Remember that the jobs are defined as top-level elements with a
   name, here "linux", and always have to contain at least the *script*
   clause. Here we define a simple job which configure, build and
   execute tests with the last state of the code on the branch we push
   on gitlab.

   Commit, push and visit the gitlab page of your project,
   subsection CI/CD -> Pipelines. You should see that a pipeline has
   started and you can clic on it (rectangle under the /Status/ column)
   to follow the step and check job logs (clic on the job round
   sticker).

   Now let's define two jobs which will build two configurations, here
   compilations with two different C compilers GCC and Clang:
   #+begin_example
   linux-gcc:
     variables:
       CC: gcc
     script:
       - cmake .
       - make
       - ctest

   linux-clang:
     variables:
       CC: clang
     script:
       - cmake .
       - make
       - ctest
   #+end_example
   Remarks:
   1. By default all jobs are considered independent and can be run in
      parallel if several runners are available. If you have only one
      runner the jobs will be run one after the other. To define jobs
      that are dependent from each others, see "pipelines".
   2. We have used *variables* here to define environment variables that
      will be set during the job.

   We suggest to factorize what is generic here by using some [[https://gitlab.inria.fr/help/ci/yaml/README.md#special-yaml-features][yaml
   special features]] which allow to write macros:
   #+begin_example
   .build_test_script: &build_test_script
     script:
       - cmake .
       - make
       - ctest

   linux-gcc:
     variables:
       CC: gcc
     <<: *build_test_script

   linux-clang:
     variables:
       CC: clang
     <<: *build_test_script
   #+end_example
   Here the content of the "build_test_script" macro is copied where
   it is necessary.

** define multiple jobs as a pipeline (jobs organized as a sequential chain)
   References
   - https://gitlab.inria.fr/help/ci/yaml/README.md#stages
   - https://gitlab.inria.fr/help/ci/pipelines.md

   Jobs are conceptually gathered in *stages* (default: build -> test ->
   deploy). We can define a sequence of stages, e.g. stage1, stage2,
   stage3, and jobs can be affected to one of these stages. All jobs
   in a same stage will run independently in parallel but the jobs of
   the stage2 will wait for the completion of stage1 jobs to start
   (stage3 jobs wait for stage2 jobs completion). If no *stage* keyword
   is used in a job it will be affected in the arbitrary default stage
   named "test".

   Stages are convenient because we define coherent set of jobs with
   dependencies between them. For example a stage for builds, one for
   tests, and if tests succeed we could build and publish the
   documentation, an archive of the source code, etc.

   #+begin_example
   stages:
   - build
   - test
   - other

   build:
     stage: build
     script:
       - cmake . && make

   test:
     stage: test
     script:
       - cmake . && make && ctest

   documentation:
     stage: other
     script:
       - cmake . -DHEAT_DOC=ON && make doc
   #+end_example
   Remarks:
   1. There is only one job per stage here but there can be several
      and they would run in parallel if several runners were set.
   2. Notice that by default all jobs have their own environment and
      work on a clean git repository. This means that even if the
      "build" job has always performed the build, the job "test" don't
      have the result of this build /i.e./ the binaries. That is why we
      build also the project in the "test" job here. To save the
      results of a job one has to define *artifacts* or use *cache*. We
      discuss these features in the next section.

** artifacts/cache
   References:
   - https://gitlab.inria.fr/help/ci/yaml/README.md#artifacts
   - https://gitlab.inria.fr/help/ci/yaml/README.md#cache

   To share some non versionned files between subsequent jobs there
   are two possibilities *artifacts* or *cache*.

*** Artifacts
    Artifacts are normally used to save some content, generated during
    a job, in the form of a zip archive uploaded on the web server
    (attached to the job in the CI/CD -> Pipelines and Jobs page). In
    addition to that the artifact can be transmitted to a job of
    following stages.

    #+begin_example
    stages:
    - build
    - test

    before_script:
      - mkdir -p build

    build:
      artifacts:
        paths:
          - build
      stage: build
      script:
        - cd build && cmake .. && make

    test:
      stage: test
      script:
        - cd build && ctest
    #+end_example
    *before_script* keyword is used to define some commands to execute
    before the script of the jobs. Here it is defined globally so that
    it will be done for all jobs. If *before_script* is used locally it
    will not use the global before_script.

    By default all artifacts from previous stages are downloaded by
    jobs. To constrain to a specific artifact one has to use the
    *dependencies* keyword and give the specific jobs we want to
    depend on, /e.g./ here
    #+begin_example
    test:
      stage: test
      dependencies:
        - build
      script:
        - cd build && ctest
    #+end_example

    By default artifacts are removed after 30 days. This can be
    changed with the keyword "expire_in"
    #+begin_example
    build:
      artifacts:
        expire_in: 1 day
        paths:
          - build
      stage: build
      script:
        - cd build && cmake .. && make
    #+end_example
    Artifacts can also be kept forever, see the "Keep" button on the
    job page.

    Tips:
    - The archive name can be chosen with the *name* keyword, /e.g./ name:
      "$CI_JOB_NAME".
    - *untracked: true* can be used in place of *paths* to consider all
      unversionned files (apart from the content of .gitignore)
    #+begin_example
    artifacts:
      expire_in: 1 day
      name: "$I_JOB_NAME"
      untracked: true
    #+end_example

    Remark: artifacts can be downloaded by jobs on all the runners so
    that related jobs in the pipeline are not necessarily affected to
    a specific runner which is flexible.

    *Limitation*: there exists a size limit for the artifacts which is
    500 MB currently on the Inria Gitlab server. Thus they can't be
    used to transfer large dataset between jobs such as all the
    binaries for large compilations, cache should be used instead.

*** Cache
    Cache allows to define some paths (files and directories) within
    the project workspace which should be cached between jobs and even
    between pipelines.

   #+begin_example
   stages:
   - build
   - test

   cache:
     untracked: true

   build:
     stage: build
     script:
       - cmake . && make

   test:
     stage: test
     script:
       - ctest
   #+end_example
   Remarks:
   1. Here all unversioned files will be cached in a directory such as
      ~/home/gitlab-runner/cache/project-namespace/project-name/cache-key~
      of the runner.
   2. Cache is used globally here so that all jobs will be able to
      read/write the cache. If a subset of jobs should use the cache
      define it locally at the jobs level.
   3. The cache is active between pipelines. This means that the
      results of these jobs are kept for the next pipeline. If a
      commit do not affect source code for example the build process
      will be very fast because targets will be up-to-date.
   4. Parameters that can be tuned for cache
      - *paths*: directories or files to cache
      - *key*: to name a cache more specifically (default name is
        "default") to avoid mixing cache of different jobs, branches,
        etc
      - *policy*: to control if cache should be pulled and pushed, just
        pulled or just pushed

   *Limitation*: caching makes sense on a single machine /i.e./ a runner
   so that jobs that share their cache should use the same runner. We
   discuss how to affect jobs to some specific runners with tags in
   the next section.

   To learn more about cache please read these [[https://gitlab.inria.fr/help/ci/caching/index.md][good caching pratices]].

** execute on different systems with tags
   References:
   - https://gitlab.inria.fr/help/ci/yaml/README.md#tags

   Tags allow to constrain jobs to run on some specific runners. When
   registering runners a list of tags can be given. If one of these
   tags is given to a job it will force the execution of the job on
   one runner owning the tag. It is convenient to identify different
   systems such as Linux, Mac OS X, Windows, on which build script may
   not be the same for instance. It should also be considered when the
   cache mechanism is used in order to stick some jobs to a specific
   runner.

   #+begin_example
   build-linux:
     tags:
       - linux
     script:
       - cmake .
       - make

   build-windows:
     tags:
       - windows
     script:
       - cmake . -G"MSYS Makefiles"
       - make
   #+end_example

** use docker images
   References:
   - https://gitlab.inria.fr/help/ci/yaml/README.md#image-and-services
   - https://gitlab.inria.fr/help/ci/docker/using_docker_images.md

   To perform continuous integration on some machines the dependencies
   of the program to test must be installed on each system that will
   serve as gitlab runner. This can be a long and cumbersome task
   making the process of adding/removing runners not flexible. Docker
   images allows to build an environment ready to be used to test the
   application. Once the image is built and uploaded on a server, a
   runner can download it on any compatible machine and perform
   gitlab-ci jobs inside this preconceived environment instead of the
   runner one. This makes things clearer because we know exactly on
   which environment we rely on (improve reproducibility) and adding
   new runners is faster because we don't need to set the environment
   and install all dependencies (just gitlab-runner and docker).

   To tell gitlab-ci to perform jobs in a Docker image, the keyword is
   *image*. It can be set globally (all jobs will be concerned) or
   locally on some specific jobs.

   Here is an example with a global definition:
   #+begin_example
   image: registry.gitlab.inria.fr/sed-bso/heat

   cache:
     untracked: true

   build:
     stage: build
     tags: ["docker"]
     script: cmake . && make

   test:
     stage: test
     tags: ["docker"]
     script: ctest
   #+end_example
   Remarks:
   1. We use the "docker" tag to perform the jobs on a runner with
      docker executor, see [[http://docs.gitlab.com/runner/register/index.html][register runner]].
   2. We use an image that we have built ourself and pushed on the
      gitlab server (registry of the project "sed-bso/heat"). Every
      project has a registry where Docker images can be stored, see
      [[https://gitlab.inria.fr/help/user/project/container_registry.md][container registry]].
   3. cache can still be used with Docker images.

** tune jobs creation and status (git branches/refs, on failure/success)
   References:
   - https://gitlab.inria.fr/help/ci/yaml/README.md#only-and-except-simplified
   - https://gitlab.inria.fr/help/ci/yaml/README.md#when
   - https://gitlab.inria.fr/help/ci/yaml/README.md#allow_failure

   By default jobs in gitlab-ci are created if:
   1. they are defined in the ~.gitlab-ci.yml~ file, note this file
      could be present in the different branches and in forks
   2. all jobs of the previous stage succeed (if stages are used)
   These default behaviors can be changed.

   One can constrain jobs to be created only for some specific git
   refs (branches/tags):

   #+begin_example
   cache:
     untracked: true

   build:
     stage: build
     script: cmake . && make

   test-master:
     stage: test
     only:
       - master
     script: ctest

   test-branches:
     stage: test
     only:
       - branches
     except:
       - master
     script: ctest -R heat_seq

   documentation:
     stage: deploy
     allow_failure: true
     when: always
     script: ["cmake . -DHEAT_DOC=ON", "make doc"]
   #+end_example
   Remarks:
   1. Here "build" job is always performed, "test-master" is only
      performed on the master branch and "test-branches" is performed
      on all branches which are not master.
   2. We can limit jobs on the parent repository and not forks, /e.g./
      ~master@sed-bso/heat~.
   3. Regular expressions can be used to filter git refs /e.g./
      ~^release-.*$~.
   4. In addition to specific refs *only* and *except* accept keywords to
      indicate some groups of refs or actions which should trigger the
      pipeline, see *branches*, *tags*, *api*, *pushes*, *schedules*, *web*, etc
   5. *allow_failure* allows to create jobs that will not interfere in
      the pipeline overall status
   6. *when* is used to force the execution policy
      - on_success: default policy, execute when all jobs from the prior
        stages succeed
      - on_failure: only if at least one job from prior stages fails
      - always: execute job regardless of the status from prior stages
      - manual: execute job manually, /e.g./ in CI/CD -> Pipelines ->
        Run Pipeline (green round sticker)

** use pages to publish the project website and/or some documentation

   Reference: https://gitlab.inria.fr/help/user/project/pages/index.md

   GitLab Pages is a hosting service for static websites. It allows to
   define a gitlab-ci jobs, called *pages*, that will generate the
   website files. All the files must be copied in a directory "public"
   of the project workspace, directory which must be defined as
   artifact. The files will be automatically uploaded to a web server
   with a url related to the project's name
   (https://.gitlabpages.inria.fr/<repository>).

   For example for the [[https://gitlab.inria.fr/sed-bso/heat][heat]] project, the following job *pages* allows to
   upload this [[https://sed-bso.gitlabpages.inria.fr/heat/gitlab-ci.html][presentation webpage]]
   #+begin_example
   pages:
     stage: deploy
     before_script:
       - mkdir public
     script:
       - cd formation/gitlab-ci
       - emacs gitlab-ci.org --batch -f org-html-export-to-html --kill
       - cp gitlab-ci.html ../../public/
       - cp -r css/ ../../public/
     artifacts:
       paths:
         - public
   #+end_example

   You can see the url of your project's *pages* in the section Settings
   -> Pages of the project.

** features that can be defined at jobs level and/or globally (for all jobs)
   - after_script: commands executed after the jobs script
   - before_script: commands executed before the jobs script
   - cache: define paths (directories and files, relative paths within
     the project workspace) to cache
   - image: docker image to use for jobs
   - (services)
   - variables: set environment variables
** configuration matrix
   Sadly, this feature is currently missing.

   What is existing is not convenient, just alias and not real
   configuration matrix feature, see
   https://gitlab.inria.fr/help/ci/yaml/README.md#special-yaml-features

   Why it is very limited, illustration
   https://gitlab.com/gitlab-org/gitlab-ce/issues/19199
   https://gitlab.com/StanfordLegion/legion/blob/master/.gitlab-ci.yml
